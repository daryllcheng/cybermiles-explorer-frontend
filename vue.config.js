module.exports = {
  devServer: {
    proxy: 'http://127.0.0.1:7001',
    // proxy: 'http://192.168.1.25:7001',
    // proxy: 'http://192.168.1.38:7001',
  },
  /*
  pwa: {
    // configure the workbox plugin
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      swSrc: 'public/service-worker.js',
      // ...other Workbox options...
    },
  },
  */
};
