import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import router from './router';

// window.axios = require('axios');

// window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

require('./tools/errorHandler');

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    transactions: [],
    totalTransactions: null,
    totalTransactionsInDB: null,
    blocks: [],
    totalBlocks: null,
    totalBlocksInDB: null,
    accounts: [],
    totalAccounts: null,
    totalAccountsInDB: null,
    header: {},
    nodes: [],
    tokens: [],
    totalTokens: null,
    contracts: [],
    totalContracts: null,
    node: {},
    token: {},
    height: null,
    transaction: {},
    block: {},
    address: {},
    blockTransactions: [],
    totalBlockTransactions: null,
    addressTransactions: [],
    totalAddressTransactions: null,
    tokenTransactions: [],
    totalTokenTransactions: null,
    addressTokens: [],
    totalAddressTokens: null,
    addressStakes: [],
    totalAddressStakes: null,
    nodeDelegators: [],
    totalNodeDelegators: null,
    nodeDelegatorsStakeSum: null,
    stakeRewards: [],
    totalStakeRewards: null,
    fetchError: false,
    missingBlocks: null,
    missingTransactions: null,
    missingContracts: null,
    accountBalance: {},
  },
  getters: {
    transactions: state => state.transactions,
    totalTransactions: state => state.totalTransactions,
    totalTransactionsInDB: state => state.totalTransactionsInDB,
    blocks: state => state.blocks,
    totalBlocks: state => state.totalBlocks,
    totalBlocksInDB: state => state.totalBlocksInDB,
    accounts: state => state.accounts,
    totalAccounts: state => state.totalAccounts,
    totalAccountsInDB: state => state.totalAccountsInDB,
    header: state => state.header,
    nodes: state => state.nodes.sort((a, b) => a.rank - b.rank),
    tokens: state => state.tokens,
    totalTokens: state => state.totalTokens,
    contracts: state => state.contracts,
    totalContracts: state => state.totalContracts,
    node: state => state.node,
    token: state => state.token,
    height: state => state.height,
    transaction: state => state.transaction,
    block: state => state.block,
    address: state => state.address,
    blockTransactions: state => state.blockTransactions,
    totalBlockTransactions: state => state.totalBlockTransactions,
    addressTransactions: state => state.addressTransactions,
    totalAddressTransactions: state => state.totalAddressTransactions,
    tokenTransactions: state => state.tokenTransactions,
    totalTokenTransactions: state => state.totalTokenTransactions,
    addressTokens: state => state.addressTokens,
    totalAddressTokens: state => state.totalAddressTokens,
    addressStakes: state => state.addressStakes,
    totalAddressStakes: state => state.totalAddressStakes,
    nodeDelegators: state => state.nodeDelegators,
    totalNodeDelegators: state => state.totalNodeDelegators,
    stakeRewards: state => state.stakeRewards,
    totalStakeRewards: state => state.totalStakeRewards,
    nodeDelegatorsStakeSum: state => state.nodeDelegatorsStakeSum,
    fetchError: state => state.fetchError,
    missingBlocks: state => state.missingBlocks,
    missingTransactions: state => state.missingTransactions,
    missingContracts: state => state.missingContracts,
    accountBalance: state => state.accountBalance,
  },
  mutations: {
    POPULATE_HOME(state, data) {
      state.transactions = data.data.lastTenTransactions;
      state.blocks = data.data.lastTenBlocks;
      state.header = data.data.hash_data;
    },
    POPULATE_TRANSACTIONS(state, data) {
      state.transactions = data.data.objects;
      state.totalTransactions = data.data.meta.total_limit;
      state.totalTransactionsInDB = data.data.meta.total;
    },
    POPULATE_BLOCKS(state, data) {
      state.blocks = data.data.objects;
      state.totalBlocks = data.data.meta.total_limit;
      state.totalBlocksInDB = data.data.meta.total;
    },
    POPULATE_ACCOUNTS(state, data) {
      state.accounts = data.data.objects;
      state.totalAccounts = data.data.meta.total_limit;
      state.totalAccountsInDB = data.data.meta.total;
    },
    POPULATE_HEADER(state, data) {
      state.header = data.data;
    },
    POPULATE_NODES(state, data) {
      state.nodes = data.data;
      state.height = data.height;
    },
    POPULATE_TOKENS(state, data) {
      state.tokens = data.data.objects;
      state.totalTokens = data.data.meta.total;
      state.totalTokensInDB = data.data.meta.total_limit;
    },
    POPULATE_CONTRACTS(state, data) {
      state.contracts = data.data.objects;
      state.totalContracts = data.data.meta.total;
      state.totalContractsInDB = data.data.meta.total_limit;
    },
    LOAD_TRANSACTION(state, data) {
      state.transaction = data;
    },
    LOAD_BLOCK(state, data) {
      state.block = data;
    },
    LOAD_ADDRESS(state, data) {
      state.address = data;
    },
    LOAD_NODE(state, node) {
      state.node = node;
    },
    LOAD_TOKEN(state, data) {
      state.token = data;
    },
    POPULATE_BLOCK_TRANSACTIONS(state, data) {
      state.blockTransactions = data.data.objects;
      state.totalBlockTransactions = data.data.meta.total;
    },
    POPULATE_ADDRESS_TRANSACTIONS(state, data) {
      state.addressTransactions = data.data.objects;
      state.totalAddressTransactions = data.data.meta.total;
    },
    POPULATE_TOKEN_TRANSACTIONS(state, data) {
      state.tokenTransactions = data.data.objects;
      state.totalTokenTransactions = data.data.meta.total;
    },
    POPULATE_ADDRESS_TOKENS(state, data) {
      state.addressTokens = data.data.objects;
      state.totalAddressTokens = data.data.meta.total;
    },
    POPULATE_ADDRESS_STAKES(state, data) {
      state.addressStakes = data.data.objects.data;
      state.totalAddressStakes = data.data.meta.total;
    },
    POPULATE_NODE_DELEGATORS(state, data) {
      state.nodeDelegators = data.data.objects;
      state.totalNodeDelegators = data.data.meta.total_limit;
      // state.nodeDelegatorsStakeSum = data.data.meta.stakeSum;
    },
    POPULATE_STAKE_REWARDS(state, data) {
      state.stakeRewards = data.data.objects;
      state.totalStakeRewards = data.data.meta.total_limit;
    },
    TRIGGER_FETCH_ERROR(state) {
      state.fetchError = true;
    },
    RESET_ADDRESS_TOTALS(state) {
      state.totalAddressTransactions = null;
      state.totalTokenTransactions = null;
      state.totalAddressTokens = null;
      state.totalAddressStakes = null;
      state.totalStakeRewards = null;
    },
    POPULATE_MISSING_BLOCKS(state, data) {
      state.missingBlocks = data.data;
    },
    POPULATE_MISSING_TRANSACTIONS(state, data) {
      state.missingTransactions = data.data;
    },
    POPULATE_MISSING_CONTRACTS(state, data) {
      state.missingContracts = data.data;
    },
    LOAD_ACCOUNT_BALANCE(state, data) {
      state.accountBalance = data.data;
    },
  },
  actions: {
    showLoader: ({ commit }, { status }) => {
      commit('SHOW_LOADER', status);
    },
    fetchHomeData: ({ commit }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/v3/homeData`)
        .then((response) => {
          commit('POPULATE_HOME', response.data);
        })
        .catch((err) => {
          commit('TRIGGER_FETCH_ERROR');
          console.error(err);
        }),
    fetchTransactions: ({ commit }, { limit, page }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/v3/transactions?limit=${limit}&page=${page}`)
        .then((response) => {
          commit('POPULATE_TRANSACTIONS', response.data);
        })
        .catch((err) => {
          commit('TRIGGER_FETCH_ERROR');
          console.error(err);
        }),
    fetchBlocks: ({ commit }, { limit, page }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/v3/blocks?limit=${limit}&page=${page}`)
        .then((response) => {
          commit('POPULATE_BLOCKS', response.data);
        })
        .catch((err) => {
          commit('TRIGGER_FETCH_ERROR');
          console.error(err);
        }),
    fetchAccounts: ({ commit }, { limit, page }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/v3/accounts?limit=${limit}&page=${page}`)
        .then((response) => {
          commit('POPULATE_ACCOUNTS', response.data);
        })
        .catch((err) => {
          commit('TRIGGER_FETCH_ERROR');
          console.error(err);
        }),
    fetchHeader: ({ commit }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/getHashInfo`)
        .then((response) => {
          commit('POPULATE_HEADER', response.data);
        })
        .catch((err) => {
          commit('TRIGGER_FETCH_ERROR');
          console.error(err);
        }),
    fetchNodes: ({ commit }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/nodes`)
        .then((response) => {
          commit('POPULATE_NODES', response.data);
        })
        .catch((err) => {
          commit('TRIGGER_FETCH_ERROR');
          console.error(err);
        }),
    fetchTokens: ({ commit }, { limit, page }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/v3/tokens?limit=${limit}&page=${page}`)
        .then((response) => {
          commit('POPULATE_TOKENS', response.data);
        })
        .catch((err) => {
          commit('TRIGGER_FETCH_ERROR');
          console.error(err);
        }),
    fetchContracts: ({ commit }, { limit, page }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/v3/contracts?limit=${limit}&page=${page}`)
        .then((response) => {
          commit('POPULATE_CONTRACTS', response.data);
        })
        .catch((err) => {
          commit('TRIGGER_FETCH_ERROR');
          console.error(err);
        }),
    fetchBlockTransactions: ({ commit }, { block, limit, page }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/v3/blockTransactions?block=${block}&limit=${limit}&page=${page}`)
        .then((response) => {
          commit('POPULATE_BLOCK_TRANSACTIONS', response.data);
        })
        .catch((err) => {
          console.error(err);
        }),
    fetchAddressTransactions: ({ commit }, { address, limit, page }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/v3/addressTransactions?address=${address}&limit=${limit}&page=${page}`)
        .then((response) => {
          commit('POPULATE_ADDRESS_TRANSACTIONS', response.data);
        })
        .catch((err) => {
          console.error(err);
        }),
    fetchTokenTransactions: ({ commit }, { address, limit, page }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/v3/tokenTransactions?address=${address}&limit=${limit}&page=${page}`)
        .then((response) => {
          commit('POPULATE_TOKEN_TRANSACTIONS', response.data);
        })
        .catch((err) => {
          console.error(err);
        }),
    fetchAddressTokens: ({ commit }, { address, limit, page }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/v3/addressTokens?address=${address}&limit=${limit}&page=${page}`)
        .then((response) => {
          commit('POPULATE_ADDRESS_TOKENS', response.data);
        })
        .catch((err) => {
          console.error(err);
        }),
    fetchAddressStakes: ({ commit }, { address, limit, page }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/v3/addressStakes?address=${address}&limit=${limit}&page=${page}`)
        .then((response) => {
          commit('POPULATE_ADDRESS_STAKES', response.data);
        })
        .catch((err) => {
          console.error(err);
        }),
    fetchNodeDelegators: ({ commit }, { address, limit, page }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/v3/nodeDelegators?address=${address}&limit=${limit}&page=${page}`)
        .then((response) => {
          commit('POPULATE_NODE_DELEGATORS', response.data);
        })
        .catch((err) => {
          console.error(err);
        }),
    fetchStakeRewards: ({ commit }, { address, limit, page }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/v3/addressStakeRewardHistory?address=${address}&limit=${limit}&page=${page}`)
        .then((response) => {
          commit('POPULATE_STAKE_REWARDS', response.data);
        })
        .catch((err) => {
          console.error(err);
        }),
    fetchMissingBlocks: ({ commit }, { floor, ceiling }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/checkMissedBlock?startHeight=${floor}&endHeight=${ceiling}`)
        .then((response) => {
          commit('POPULATE_MISSING_BLOCKS', response.data);
        })
        .catch((err) => {
          console.error(err);
        }),
    fetchMissingTransactions: ({ commit }, { floor, ceiling }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/checkMissedTransaction?startHeight=${floor}&endHeight=${ceiling}`)
        .then((response) => {
          commit('POPULATE_MISSING_TRANSACTIONS', response.data);
        })
        .catch((err) => {
          console.error(err);
        }),
    fetchMissingContracts: ({ commit }, { floor, ceiling }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/checkMissedContract?startHeight=${floor}&endHeight=${ceiling}`)
        .then((response) => {
          commit('POPULATE_MISSING_CONTRACTS', response.data);
        })
        .catch((err) => {
          console.error(err);
        }),
    checkAccountBalance: ({ commit }, { address }) =>
      axios.get(`${process.env.VUE_APP_API_ENDPOINT}/checkAccountBalance?address=${address}`)
        .then((response) => {
          commit('LOAD_ACCOUNT_BALANCE', response.data);
        })
        .catch((err) => {
          console.error(err);
        }),
    resetAddressTotals: ({ commit }) => commit('RESET_ADDRESS_TOTALS'),
    submitQuery: ({ commit }, { query, type = '' }) => {
      const req = /^\d+$/;
      let queryType = 'block';
      if (type === '') {
        if (req.test(query)) {
          queryType = 'block';
        } else if (query.length === 42) {
          queryType = 'address';
        } else if (query.length <= 5) {
          queryType = 'token';
        } else {
          queryType = 'txhash';
        }
      } else {
        queryType = type;
      }
      return axios.get(`${process.env.VUE_APP_API_ENDPOINT}/v3/search?${queryType}=${query}`)
        .then((response) => {
          if (queryType === 'block') {
            commit('LOAD_BLOCK', response.data || {});
            router.push(`/block/${query}`);
          } else if (queryType === 'address') {
            commit('LOAD_ADDRESS', response.data || {});
            router.push(`/address/${query}`);
          } else if (queryType === 'txhash') {
            commit('LOAD_TRANSACTION', response.data || {});
            router.push(`/tx/${query}`);
          } else if (queryType === 'token') {
            commit('LOAD_TOKEN', response.data);
            router.push(`/token/${query}`);
          } else if (queryType === 'node') {
            commit('LOAD_NODE', response.data || {});
          }
        })
        .catch((err) => {
          console.error(err);
        });
    },
  },
});
