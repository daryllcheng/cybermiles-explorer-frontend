import Vue from 'vue';
import Vuex from 'vuex';
import mockTransactions from './assets/mockData/transactions.json';
import mockBlocks from './assets/mockData/blocks.json';
import mockHeader from './assets/mockData/header.json';
import mockNodes from './assets/mockData/nodes.json';
import mockAddress from './assets/mockData/address.json';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    transactions: [],
    totalTransactions: null,
    blocks: [],
    totalBlocks: null,
    header: {},
    nodes: [],
    node: {},
    height: null,
    transaction: {},
    block: {},
    address: {},
    loader: false,
  },
  getters: {
    transactions: state => state.transactions,
    totalTransactions: state => state.totalTransactions,
    blocks: state => state.blocks,
    totalBlocks: state => state.totalBlocks,
    header: state => state.header,
    nodes: state => state.nodes,
    node: state => state.node,
    height: state => state.height,
    transaction: state => state.transaction,
    block: state => state.block,
    address: state => state.address,
    loader: state => state.loader,
  },
  mutations: {
    POPULATE_TRANSACTIONS(state, data) {
      state.transactions = data.data;
      state.totalTransactions = data.total;
    },
    POPULATE_BLOCKS(state, data) {
      state.blocks = data.data;
      state.totalBlocks = data.total;
    },
    POPULATE_HEADER(state, data) {
      state.header = data;
    },
    POPULATE_NODES(state, data) {
      state.nodes = data.data;
      state.height = data.height;
    },
    LOAD_TRANSACTION(state, data) {
      state.transaction = data;
    },
    LOAD_BLOCK(state, data) {
      state.block = data;
    },
    LOAD_ADDRESS(state, data) {
      state.address = data;
    },
    LOAD_NODE(state, node) {
      state.node = node;
    },
    SHOW_LOADER(state, status) {
      state.loader = status;
    },
  },
  actions: {
    showLoader: ({ commit }, { status }) => {
      commit('SHOW_LOADER', status);
    },
    fetchTransactions: ({ commit, dispatch }, { limit, page }) => {
      dispatch('showLoader', { status: true });
      commit('POPULATE_TRANSACTIONS', mockTransactions);
      dispatch('showLoader', { status: false });
    },
    fetchBlocks: ({ commit, dispatch }, { limit, page }) => {
      dispatch('showLoader', { status: true });
      // Mock Data
      commit('POPULATE_BLOCKS', mockBlocks);
      dispatch('showLoader', { status: false });
    },
    fetchHeader: ({ commit, dispatch }) => {
      dispatch('showLoader', { status: true });
      commit('POPULATE_HEADER', mockHeader);
      dispatch('showLoader', { status: false });
    },
    fetchNodes: ({ commit, dispatch }) => {
      dispatch('showLoader', { status: true });
      commit('POPULATE_NODES', mockNodes);
      dispatch('showLoader', { status: false });
    },
    loadNode: ({ commit }, { node }) => {
      commit('LOAD_NODE', JSON.parse(node));
    },
    submitQuery: ({ commit, dispatch }, { query }) => {
      dispatch('showLoader', { status: true });
      if (/^\d+$/.test(query)) {
        commit('LOAD_BLOCK', mockBlocks.data[0]);
      } else if (query.length === 42) {
        commit('LOAD_ADDRESS', mockAddress.data[0]);
      } else {
        commit('LOAD_TRANSACTION', mockTransactions.data[0]);
      }
      dispatch('showLoader', { status: false });
    },
  },
});
