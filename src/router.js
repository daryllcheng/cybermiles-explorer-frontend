import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
    },
    {
      path: '/transactions',
      name: 'transactions',
      component: () => import('./components/transactions/transactions.vue'),
    },
    {
      path: '/address/:address',
      name: 'address',
      component: () => import('./components/address/addressContainer.vue'),
    },
    {
      path: '/nodes',
      name: 'nodes',
      component: () => import('./components/nodes/nodes.vue'),
    },
    {
      path: '/accounts',
      name: 'accounts',
      component: () => import('./components/address/accounts.vue'),
    },
    {
      path: '/crc20Tokens',
      name: 'crc20Tokens',
      component: () => import('./components/tokens/crc20Tokens.vue'),
    },
    {
      path: '/blocks',
      name: 'blocks',
      component: () => import('./components/blocks/blocks.vue'),
    },
    {
      path: '/contracts',
      name: 'contracts',
      component: () => import('./components/contracts/contracts.vue'),
    },
    {
      path: '/blockRewards',
      name: 'blockRewards',
      component: () => import('./components/blockRewards/blockRewards.vue'),
    },
    {
      path: '/tx/:txhash',
      name: 'tx',
      props: true,
      component: () => import('./components/transactions/transactionContainer.vue'),
    },
    {
      path: '/block/:blockNumber',
      name: 'block',
      props: true,
      component: () => import('./components/blocks/blockContainer.vue'),
    },
    {
      path: '/blockTransactions/:blockNumber',
      name: 'blockTransactions',
      props: true,
      component: () => import('./components/blocks/blockTransactions.vue'),
    },
    {
      path: '/node/:address',
      name: 'node',
      props: true,
      component: () => import('./components/nodes/node.vue'),
    },
    {
      path: '/token/:token',
      name: 'token',
      props: true,
      component: () => import('./components/tokens/token.vue'),
    },
    {
      path: '/developer-tools',
      name: 'developer',
      props: true,
      component: () => import('./components/developer/developer.vue'),
    },
  ],
});
