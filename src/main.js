import Vue from 'vue';
import Vuetify from 'vuetify';
import ElementUI from 'element-ui';
import Buefy from 'buefy';
import VueTimeago from 'vue-timeago';
import VueClipboard from 'vue-clipboard2';

import App from './App.vue';
import router from './router';
import store from './store';

// Mock Store
// import store from './mockStore';

import './assets/scss/main.scss';
import '@babel/polyfill';
import './plugins/vuetify';
import 'element-ui/lib/theme-chalk/index.css';
import 'buefy/lib/buefy.css';
import colors from 'vuetify/es5/util/colors';

import './registerServiceWorker';

Vue.use(Vuetify);
Vue.use(Buefy);
Vue.use(ElementUI);
Vue.use(VueClipboard);
Vue.use(require('vue-moment'));


Vue.use(VueTimeago, {
  name: 'Timeago',
  locale: 'en',
  locales: {
    'zh-CN': require('date-fns/locale/zh_cn'),
  },
});

Vue.use(Vuetify, {
  theme: {
    primary: 'colors.indigo.base', // #E53935
    secondary: colors.red.lighten4, // #FFCDD2
    accent: colors.indigo.base, // #3F51B5
  },
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
