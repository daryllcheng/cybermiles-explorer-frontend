import 'izitoast/dist/css/iziToast.min.css';
import iZtoast from 'izitoast';

const toast = {
  error: (message, title = 'Error') =>
    iZtoast.error({
      title: title,
      message: message,
      position: 'bottomCenter',
    }),
  success: (message, title = 'Success') =>
    iZtoast.success({
      title: title,
      message: message,
      position: 'bottomCenter',
    }),
  copied: (message, title = 'Copied') =>
    iZtoast.info({
      title: title,
      message: message,
      position: 'bottomLeft',
    }),
};

export default toast;
